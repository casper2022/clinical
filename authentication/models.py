from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.db import models


class CustomUserManager(BaseUserManager):
    def create_user(self, email, password=None, **extra_fields):
        if not email:
            raise ValueError('The Email field must be set')

        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        return self.create_user(email, password, **extra_fields)

    def doctors(self):
        return self.filter(user_type='doctor')

    def patients(self):
        return self.filter(user_type='patient')


class User(AbstractBaseUser, PermissionsMixin):
    USER_TYPES = (
        ('doctor', 'Doctor'),
        ('patient', 'Patient'),
    )
    GENDER_TYPES = (
        ('m', 'Male'),
        ('f', 'Female'),
    )
    email = models.EmailField(unique=True)
    username = models.CharField(max_length=150, blank=True, null=True)
    first_name = models.CharField(max_length=150, blank=True)
    last_name = models.CharField(max_length=150, blank=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    contact_number = models.CharField(max_length=40, blank=True)
    gender = models.CharField(max_length=1, blank=True, choices=GENDER_TYPES, null=True)
    address = models.CharField(max_length=80, blank=True,null=True)
    user_type = models.CharField(max_length=10, choices=USER_TYPES)
    department = models.ForeignKey(
        "Department",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name='doctors'
    )

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.email


class Department(models.Model):
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name
